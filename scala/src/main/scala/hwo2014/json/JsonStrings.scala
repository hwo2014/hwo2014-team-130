package hwo2014.json

/**
  *
  * @author Michael
  */
trait JsonStrings {
   // keys
   val MSG_TYPE = "msgType"
   val DATA = "data"
   // values
   val JOIN_RACE = "joinRace"
   val CREATE_RACE = "createRace"
   val PING = "ping"
   val THROTTLE = "throttle"
   val SWITCH_LANE = "switchLane"
   val FINISH = "finish"
   val LEFT = "Left"
   val RIGHT = "Right"
   val DNF = "dnf"
   val LAP_FINISHED = "lapFinished"
   val CRASH = "crash"
   val SPAWN = "spawn"
   val TOURNAMENT_END = "tournamentEnd"
   val GAME_END = "gameEnd"
   val CAR_POSITIONS = "carPositions"
   val GAME_START = "gameStart"
   val GAME_INIT = "gameInit"
   val YOUR_CAR = "yourCar"
   val JOIN = "join"
 }

object JsonStrings extends JsonStrings