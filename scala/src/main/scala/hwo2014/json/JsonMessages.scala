package hwo2014.json

import JsonStrings._
import play.api.libs.json._

/**
 *
 * @author Michael
 */
object JsonMessages {

  case class Join(name: String, key: String)

  case class Car(name: String, color: String)

  case class GameInit(race: Race)

  case class Race(track: Track, cars: Seq[VerboseCar], raceSession: RaceSession)

  case class Track(id: String, name: String, pieces: Seq[Piece], lanes: Seq[Lane])

  case class Piece(length: Float, switch: Boolean, angle: Float)

  case class Lane(distanceFromCenter: Int, index: Int)

  case class StartingPoint(position: Position, angle: Float)

  case class Position(x: Float, y: Float)

  case class VerboseCar(id: Car, dimensions: CarDimensions)

  case class CarDimensions(length: Float, width: Float, guideFlagPosition: Float)

  case class RaceSession(laps: Int, maxLapTimeMs: Int, quickRace: Boolean)

  case object GameStart

  case class CarPositionJson(data: Seq[CarPosition], gameId: String, gameTick: Int)

  case class CarPosition(id: Car, angle: Float, piecePosition: PiecePosition)

  case class PiecePosition(pieceIndex: Int, inPieceDistance: Float, lane: LaneInfo, lap: Int)

  case class LaneInfo(startLaneIndex: Int, endLaneIndex: Int)

  case class ThrottleJson(data: Float)

  case class GameEnd(results: Seq[Result], bestLaps: Seq[BestLap])

  case class Result(car: Car)

  case class ResultInfo(laps: Int, ticks: Int, millis: Int)

  case class BestLap(car: Car, result: BestLapResult)

  case class BestLapResult(lap: Int, ticks: Int, millis: Int)

  case object TournamentEnd

  case class CrashJson(data: Car, gameId: String, gameTick: Int)

  case class LapFinishedJson(data: LapFinished, gameId: String, gameTick: Int)

  case class LapFinished(car: Car, lapTime: LapTime, raceTime: RaceTime, ranking: Ranking)

  case class LapTime(lap: Int, ticks: Int, millis: Int)

  case class RaceTime(laps: Int, ticks: Int, millis: Int)

  case class Ranking(overall: Int, fastestLap: Int)

  case class Dnf(car: Car, reason: String)

  case class DnfJson(data: Dnf, gameId: String, gameTick: Int)

  case class FinishJson(data: Car, gameId: String, gameTick: Int)

  case class SwitchLaneJson(data: LeftOrRight)

  case class BotId(name: String, key: String)

  case class RaceInfo(botId: BotId, trackName: String, password: String, carCount: Int)

  case class RaceInfoJson(msgType: String, data: RaceInfo)

  case class Throttle(msgType: String, data: Float)

  case class Message(msgType: String, data: JsValue)

  def createRace(raceInfo: RaceInfo) = RaceInfoJson(CREATE_RACE, raceInfo)

  def joinRace(raceInfo: RaceInfo) = RaceInfoJson(JOIN_RACE, raceInfo)

  def throttle(t: Float) = message(THROTTLE, t)

  def message[T](msgType: String, data: T)(implicit writer: Writes[T]): Message =
    Message(msgType, Json.toJson(data))

  trait LeftOrRight

  case object Left extends LeftOrRight

  case object Right extends LeftOrRight


}
