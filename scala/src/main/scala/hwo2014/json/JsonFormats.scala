package hwo2014.json

import hwo2014.json.JsonMessages._
import hwo2014.json.JsonStrings._
import play.api.libs.json.{JsResult, JsValue, Format, Json}

/**
 *
 * @author Michael
 */
object JsonFormats {
  implicit val pos = Json.format[Position]
  implicit val cd = Json.format[CarDimensions]
  implicit val lane = Json.format[Lane]
  implicit val join = Json.format[Join]
  implicit val car = Json.format[Car]
  implicit val vc = Json.format[VerboseCar]
  implicit val piece = Json.format[Piece]
  implicit val track = Json.format[Track]
  implicit val sp = Json.format[StartingPoint]
  implicit val rs = Json.format[RaceSession]
  implicit val li = Json.format[LaneInfo]
  implicit val pp = Json.format[PiecePosition]
  implicit val cp = Json.format[CarPosition]
  implicit val cpj = Json.format[CarPositionJson]
  implicit val race = Json.format[Race]
  implicit val tj = Json.format[ThrottleJson]
  implicit val res = Json.format[Result]
  implicit val resInfo = Json.format[ResultInfo]
  implicit val blr = Json.format[BestLapResult]
  implicit val bl = Json.format[BestLap]
  implicit val cj = Json.format[CrashJson]
  implicit val lt = Json.format[LapTime]
  implicit val rc = Json.format[RaceTime]
  implicit val r = Json.format[Ranking]
  implicit val lf = Json.format[LapFinished]
  implicit val lfj = Json.format[LapFinishedJson]
  implicit val dmf = Json.format[Dnf]
  implicit val dnfJson = Json.format[DnfJson]
  implicit val fJson = Json.format[FinishJson]
  implicit val bi = Json.format[BotId]
  implicit val gameInit = Json.format[GameInit]
  implicit val ri = Json.format[RaceInfo]
  implicit val rij = Json.format[RaceInfoJson]
  implicit val ge = Json.format[GameEnd]

  import Json._

  implicit object LeftOrRightJson extends Format[LeftOrRight] {
    override def writes(o: LeftOrRight): JsValue =
      toJson(if (o == Left) LEFT else RIGHT)

    override def reads(json: JsValue): JsResult[LeftOrRight] =
      json.validate[String].map(lr => if (lr == LEFT) Left else Right)
  }

  implicit val slj = Json.format[SwitchLaneJson]

  implicit val msgFormat = Json.format[Message]
}
