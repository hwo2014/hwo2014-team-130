package hwo2014

import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import play.api.libs.json._
import hwo2014.json.JsonMessages._
import hwo2014.json.JsonStrings._
import hwo2014.json.JsonFormats._
import play.api.data.validation.ValidationError

object NoobBot extends App {

  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))

  sendJson(JOIN, Join(botName, botKey))

  private def jsonData: Iterator[JsResult[Message]] =
    Iterator.continually(reader.readLine())
      .filter(_ != null)
      .map(str => Json.parse(str).validate[Message])

  jsonData.foreach(_.fold(onError, handleMessage))

  private def onError(errors: Seq[(JsPath, scala.Seq[ValidationError])]): Unit =
    println(errors)

  private def handleMessage(msg: Message): Unit = msg.msgType match {
    case anything =>
      println(s"Received: $anything: ${msg.data}")
      sendJson(THROTTLE, 0.5)
    //    case YOUR_CAR =>
    //    case JOIN_RACE =>
    //    case CREATE_RACE =>
    //    case SWITCH_LANE =>
    //    case FINISH =>
    //    case DNF =>
    //    case LAP_FINISHED =>
    //    case CRASH =>
    //    case SPAWN =>
    //    case TOURNAMENT_END =>
    //    case GAME_START =>
    //    case GAME_END =>
    //    case PING =>
  }

  //
  //  @tailrec private def play(): Unit = {
  //    val lineOpt = Option(reader.readLine())
  //    val line = reader.readLine()
  //    if (line != null) {
  //      Serialization.read[MsgWrapper](line) match {
  //        case MsgWrapper(CAR_POSITIONS, _) =>
  //        case MsgWrapper(msgType, _) =>
  //          println("Received: " + msgType)
  //      }
  //
  //      sendMsg(message(THROTTLE, 0.5))
  //      play()
  //    }
  //  }

  def sendJson[T](msgType: String, data: T)(implicit writer: Writes[T]) =
    sendMsg(message(msgType, data))

  def sendMsg(msg: Message): Unit = send(Json.toJson(msg))

  def send(msg: JsValue): Unit = {
    writer.println(Json.stringify(msg))
    writer.flush()
  }

}